/**
 * Program that asks for two numbers to
 * the user and prints the sum of them
*/

#include <iostream>

int sum(int a, int b) {
  return a + b;
}

int main() {
  int num1, num2;

  std::cout << "Enter first number: ";
  std::cin >> num1;
  std::cout << "Enter second number: ";
  std::cin >> num2;

  int result = sum(num1, num2);

  std::cout << "The sum of " << num1 << " and " << num2 << " is " << result << std::endl;
  return 0;
}