#include <iostream>

int64_t factorial(int64_t num) {
    if (num == 0) {
        return 1;
    }
    return num * factorial(num - 1);
}

int main() {
    for (int i = 0; i < 30; i++) {
        std::cout << i << "! = " << factorial(i) << std::endl;
    }
    std::cout << "Bye!" << std::endl;
    return 0;
}