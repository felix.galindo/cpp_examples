#include <iostream>

/**
 * Function factorial
 * @param num - the number to find the factorial of
 * @return the factorial of num
 */
int factorial(int num) {
    if (num == 0) {
        return 1;
    }
    return num * factorial(num - 1);
}

int main() {
    int userInput;
    
    // Ask user for a number
    std::cout << "Enter a number: ";
    std::cin >> userInput;

    // Evaluates the factorial of the number
    int result = factorial(userInput);

    // Print the factorial of the number
    std::cout << userInput << "! = " << result << std::endl;
    return 0;
}